### 2021-10-13 - v0.1.0-Beta
 - This is the initial public release of the library.
	It implements the LatencyGG standard Interface in C# with no external dependencies.