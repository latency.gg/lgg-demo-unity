﻿using System;
using System.Runtime.InteropServices;


public enum SendMessageType : Int16
{
    sDie = -1,
    sNull = 0,
    sReady = 1,
    sUpdate = 2,
    sAck = 3

}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class SendPacket
{
    /*
         * |00 01|02 03|04 05 06 07 08 09 10 11|12 13 14 15|
         * +-----+-----+-----------------------+-----------+
         * | mVer| mTyp|         mUID          |   mSeq    |
         * +-----------+-----------+-----------+-----------+
         * |  (cont'd) |   mXPos   |   mYPos   |   mXVel   |
         * +-----------+--+--+-----+-----------+-----------+
         * |   mYVel   |et|po|UNUSD| mFoodUID  |   UNUSED  | et = mEaten; po = mDeficate; UNUSD = UNUSED
         * +-----------+--+--+-----+-----------+-----------+
         * |        mStamina       |
         * +-----------------------+
    */
    public UInt16 mVersion = 1; // 2 Octets (2)
    public SendMessageType mType; // 2 Octets (4)
    public UInt64 uid;  // 8 Octets (12)
    public UInt64 seq;  // 8 Octets (20)
    public float xPos;  // 4 Octets (24)
    public float yPos;  // 4 Octets (28)
    public float xVel;  // 4 Octets (32)
    public float yVel;  // 4 Octets (36)
    public Byte eaten;  // 1 Octets (37)
    public Byte deficate; // 1 Octets (38)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
    public Byte[] unused; // 2 Octets (40)
    public UInt32 FoodUID;  // 4 Octets (44)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public Byte[] unused2; // 4 Octets (48)
    public double stamina; // 8 Octets (56)

    public Byte[] Serialize()
    {
        UInt16 buffSize = 56;
        Byte[] output = new Byte[buffSize];
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.StructureToPtr(this, handle, false);
        Marshal.Copy(handle, output, 0, buffSize);
        Marshal.FreeHGlobal(handle);
        return output;
    }
}
