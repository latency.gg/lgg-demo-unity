﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public enum ReceiveMessageType : Int16
{
    rDie = -1,
    rNull = 0,
    rStart = 1,
    rUpdate = 2,
    rAck = 3,
    rEnd = 32766
}

public enum ObjectType : UInt16
{
    oEnemy = 0,
    oFood = 1,
    oGuano = 2,
    oPipe = 3
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class ReceivePacket  // 1010 Octets 
{
    public UInt16 mVersion; // 2
    public ReceiveMessageType mType; // 2 (4)
    public UInt64 mSeq; // 8 (14)
    public UInt16 mCount; // 2 (16)
    public Byte   mRequireAck; // 1 (17)
    public Byte unused; // 1 (18)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 992)] // 31 Updates
    public Byte[] updates;

    public static ReceivePacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 1010;
        ReceivePacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (ReceivePacket)Marshal.PtrToStructure(handle, typeof(ReceivePacket));
        return received;
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class UpdateSubPacket
{
    public ObjectType mType;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
    public Byte[] mUpdate;
    public static UpdateSubPacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 32;
        UpdateSubPacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (UpdateSubPacket)Marshal.PtrToStructure(handle, typeof(UpdateSubPacket));
        return received;
    }
}



[StructLayout(LayoutKind.Sequential, Pack = 1)]
class EnemyPacket
{
    public UInt64 uid;  // 8 Octets
    public float xPos;  // 4 Octets (12)
    public float yPos;  // 4 Octets (16)
    public float xVel;  // 4 Octets (20)
    public float yVel;  // 4 Octets (24)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    Byte[] UNUSED;
    public static EnemyPacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 30;
        EnemyPacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (EnemyPacket)Marshal.PtrToStructure(handle, typeof(EnemyPacket));
        return received;
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class FoodPacket
{
    public UInt32 uid;  // 4 Octets
    public float xPos;  // 4 Octets (4)
    public float yPos;  // 4 Octets (8)
    public Byte consumed; // 1 Octets
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
    Byte[] UNUSED;
    public static FoodPacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 30;
        FoodPacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (FoodPacket)Marshal.PtrToStructure(handle, typeof(FoodPacket));
        return received;
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class GuanoPacket
{
    public float xPos;  // 4 Octets (4)
    public float yPos;  // 4 Octets (8)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 22)]
    Byte[] UNUSED;

    public static GuanoPacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 30;
        GuanoPacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (GuanoPacket)Marshal.PtrToStructure(handle, typeof(GuanoPacket));
        return received;
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
class PipePacket
{
    public float xPos;  // 4 Octets (4)
    public float yPos;  // 4 Octets (8)
    public float scale; // 4 Octets (12)
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
    Byte[] UNUSED;

    public static PipePacket Deserialize(Byte[] aData)
    {
        UInt16 buffSize = 30;
        PipePacket received;
        IntPtr handle = Marshal.AllocHGlobal(buffSize);
        Marshal.Copy(aData, 0, handle, buffSize);
        received = (PipePacket)Marshal.PtrToStructure(handle, typeof(PipePacket));
        return received;
    }
}