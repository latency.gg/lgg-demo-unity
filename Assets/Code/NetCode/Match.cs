using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;

using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

public class Match : MonoBehaviour
{
    public GameObject StatusText;
    public string ticketID;
    public string url = "http://127.0.0.1:5000/ticket";
    double timeSince = 0.0;
    int errors = 0;
    // Start is called before the first frame update
    void Start()
    {
        GameAPI apiResponse;
        try { 
            using (WebClient client = new WebClient())
            {
                string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(PlayerPrefs.GetString("sUID") + ":" + PlayerPrefs.GetString("password")));
                client.Headers[HttpRequestHeader.Authorization] = string.Format(
                    "Basic {0}", credentials);
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                string response = client.UploadString(new Uri(url), "POST", "");
                apiResponse = JsonConvert.DeserializeObject<GameAPI>(response);
                ticketID = apiResponse.id;
            }
        }
        catch (WebException e)
        {
            HttpWebResponse response = (HttpWebResponse)e.Response;
            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                SceneManager.LoadScene("PreferencesMenu");
            }
            else
            {
                Debug.Log("Error contacting API" + e.Message + " " + e.Status);
                CancelButton();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timeSince > 0.1)
        {
            GameAPI apiResponse;
            try {
                using (WebClient client = new WebClient())
                {
                    string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(PlayerPrefs.GetString("sUID") + ":" + PlayerPrefs.GetString("password")));
                    client.Headers[HttpRequestHeader.Authorization] = string.Format(
                        "Basic {0}", credentials);
                    string response = client.DownloadString(new Uri(url));
                    apiResponse = JsonConvert.DeserializeObject<GameAPI>(response);
                }
                ticketID = apiResponse.id;
                if (apiResponse.ready)
                {
                    MultiPlayerStatics.Host = apiResponse.host;
                    MultiPlayerStatics.Port = apiResponse.port;
                    MultiPlayerStatics.UID = apiResponse.uid;
                    SceneManager.LoadScene(MultiPlayerStatics.SelectedScene);
                }
            }
            catch (WebException e)
            {
                Debug.Log("Error contacting API: "+ e.Message);
                errors++;
            }
            timeSince = 0.0;
        } else
        {
            timeSince += Time.deltaTime;
        }
        if (errors > 3)
        {
            CancelButton();
        }
    }

    public void CancelButton()
    {
        GameAPI apiResponse;
        try
        {
            using (WebClient client = new WebClient())
            {
                string credentials = Convert.ToBase64String(
            Encoding.ASCII.GetBytes(PlayerPrefs.GetString("sUID") + ":" + PlayerPrefs.GetString("password")));
                client.Headers[HttpRequestHeader.Authorization] = string.Format(
                    "Basic {0}", credentials);
                client.UploadString(new Uri(url), "DELETE", "");
            }
        }
        catch (WebException e)
        {
            Debug.Log("Error contacting API: " + e.Message);
            errors++;
        }
        SceneManager.LoadScene("MultiPlayerMenu");
    }
}



[System.Serializable]
public class GameAPI
{
    public string   id;
    public bool     ready;
    public string   host;
    public int      port;
    public UInt64   uid;
}