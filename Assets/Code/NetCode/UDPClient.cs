using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class ActionFunc1u : UnityEvent<UInt32> { }

[Serializable]
public class ActionFunc1v : UnityEvent<Vector2> { }

[Serializable]
public class ActionFunc2f : UnityEvent<float, float> { }

[Serializable]
public class ActionFunc1U : UnityEvent<UInt64> { }

[Serializable]
public class ActionFunc1u2v : UnityEvent<UInt64, Vector2, Vector2> { }

public class UDPClient : MonoBehaviour
{
    public GameObject? player;
    public GameObject game;

    public GameObject FoodPrefab;
    public ActionFunc1u RemoveFood;

    public GameObject PipePrefab;

    public GameObject EnemyPrefab;

    public ActionFunc1u2v UpdateEnemy;

    private bool connected = false;
    public bool changed = false;
    private Socket s;
    private UInt16 gameState = 0;
    private UInt64 seq = 0;
    private float timeSince = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!connected)
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
            {
                ReceiveBufferSize = 1011
            };
            connected = true;
        }
        while (s.Available >= 48)
        {
            Byte[] bytesReceived = new Byte[1011];
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint senderRemote = (EndPoint)sender;
            int bytesRecv = s.ReceiveFrom(bytesReceived, ref senderRemote);
            sender = (IPEndPoint)senderRemote;
            if (sender.Address.ToString() == MultiPlayerStatics.Host)
            {
                ReceivePacket incomming = ReceivePacket.Deserialize(bytesReceived);
                if (incomming.mVersion == 1) {
                    ProcessPacket(incomming);
                }
                if (incomming.mRequireAck == 1)
                {
                    SendPacket packet = new SendPacket
                    {
                        uid = MultiPlayerStatics.UID,
                        seq = incomming.mSeq,
                        xPos = 0,
                        yPos = 0,
                        xVel = 0,
                        yVel = 0,
                        stamina = 0,
                        mType = SendMessageType.sAck,
                    };
                    IPAddress targetIP = IPAddress.Parse(MultiPlayerStatics.Host);
                    IPEndPoint endPoint;
                    endPoint = new IPEndPoint(targetIP, MultiPlayerStatics.Port);
                    s.SendTo(packet.Serialize(), endPoint);
                }
            }
        }
        if (changed)
        {
            Vector2 velocity = new Vector2(-10f, -10f);
            double stamina = 0.0;
            float xPos = 0.0f, yPos = 0.0f;
            if (player != null)
            {
                Birb birb = player.GetComponent<Birb>();
                velocity = birb.rb.velocity;
                stamina = birb.stamina;
                xPos = player.transform.position.x;
                yPos = player.transform.position.y;
            }

            SendPacket packet = new SendPacket
            {
                uid = MultiPlayerStatics.UID,
                seq = seq,
                xPos = xPos,
                yPos = yPos,
                xVel = velocity.x,
                yVel = velocity.y,
                stamina = stamina
            };

            if ((player != null) && (player.GetComponent<Birb>().hasEaten))
            {
                Birb birb = player.GetComponent<Birb>();
                UInt32 food = birb.eaten;
                birb.hasEaten = false;
                packet.eaten = 1;
                packet.FoodUID = food;
            }

            if ((player != null) && (player.GetComponent<Birb>().deficated))
            {
                packet.deficate = 1;
                player.GetComponent<Birb>().deficated = false;
            }

            switch (gameState)
            {
                case 0:
                    packet.mType = SendMessageType.sReady;
                    break;
                case 1:
                    packet.mType = SendMessageType.sUpdate;
                    break;
                case 2:
                    packet.mType = SendMessageType.sDie;
                    break;
                default:
                    packet.mType = SendMessageType.sNull;
                    break;
            }

            IPAddress targetIP = IPAddress.Parse(MultiPlayerStatics.Host);
            IPEndPoint endPoint;
            endPoint = new IPEndPoint(targetIP, MultiPlayerStatics.Port);
            s.SendTo(packet.Serialize(), endPoint);
            seq++;
            changed = false;
        }
        if (timeSince > 1) {
            SendPacket packet = new SendPacket
            {
                uid = MultiPlayerStatics.UID,
                seq = seq,
                xPos = 0.0f,
                yPos = 0.0f,
                xVel = 0.0f,
                yVel = 0.0f,
                stamina = 0,
                mType = SendMessageType.sNull
            };
            IPAddress targetIP = IPAddress.Parse(MultiPlayerStatics.Host);
            IPEndPoint endPoint;
            endPoint = new IPEndPoint(targetIP, MultiPlayerStatics.Port);
            s.SendTo(packet.Serialize(), endPoint);
            seq++;
            timeSince = 0.0f;
        }
        timeSince += Time.deltaTime;
    }

    public void SetGameState(UInt16 state)
    {
        gameState = state;
    }

    public UInt16 GetGameState()
    {
        return gameState;
    }

    void ProcessPacket(ReceivePacket incomming)
    {
        switch(incomming.mType)
        {
            case ReceiveMessageType.rDie:
                game.GetComponent<IMode>().Die();
                gameState = 2;
                break;
            default:
            case ReceiveMessageType.rNull:
                game.GetComponent<IMode>().Die();
                break;
            case ReceiveMessageType.rStart:
                Debug.Log("start received");
                game.GetComponent<IMode>().ClearInstructions();
                Debug.Log("spawned");
                break;
            case ReceiveMessageType.rUpdate:
                HandleUpdate(incomming.mCount, incomming.updates);
                break;
            case ReceiveMessageType.rEnd:
                Byte[] slice = incomming.updates.Take(988).ToArray();
                UpdateSubPacket sub = UpdateSubPacket.Deserialize(slice);
                EnemyPacket enemyPacket = EnemyPacket.Deserialize(sub.mUpdate);
                game.GetComponent<IMode>().NetworkEnd(enemyPacket.uid);
                gameState = 2;
                break;
        }
    }

    void HandleUpdate(UInt16 count, Byte[] blob)
    {
        UInt16 consumed = 0;
        for (int i = 0; i < count; i++)
        {
            Byte[] slice = blob.Skip(consumed).Take(consumed + 32).ToArray();
            UpdateSubPacket sub = UpdateSubPacket.Deserialize(slice);
            switch (sub.mType)
            {
                case ObjectType.oEnemy:
                    EnemyPacket enemyPacket = EnemyPacket.Deserialize(sub.mUpdate);
                    if (enemyPacket.uid != MultiPlayerStatics.UID) // I am not my own enemy
                    {
                        if (gameState == 0)
                        {
                            GameObject enemy = Instantiate(EnemyPrefab) as GameObject;
                            var enemyScript = enemy.GetComponent<Enemy>();
                            enemyScript.game = this.game;
                            enemy.transform.position = new Vector3(enemyPacket.xPos, enemyPacket.yPos, 0.0f);
                            Enemy.Enemies[enemyPacket.uid] = enemyScript;
                        }
                        else if (gameState == 1)
                        {
                            UpdateEnemy.Invoke(enemyPacket.uid, new Vector2(enemyPacket.xPos, enemyPacket.yPos), new Vector2(enemyPacket.xVel, enemyPacket.yVel));
                        }
                    }
                    break;

                case ObjectType.oFood:
                    FoodPacket foodPacket = FoodPacket.Deserialize(sub.mUpdate);
                    if (foodPacket.consumed == 0)
                    {
                        GameObject food = Instantiate(FoodPrefab) as GameObject;
                        var foodScript = food.GetComponent<Food>();
                        foodScript.game = this.game;
                        foodScript.network = true;
                        foodScript.uid = foodPacket.uid;
                        food.transform.position = new Vector3(foodPacket.xPos, foodPacket.yPos, 0.0f);
                    } 
                    else if (foodPacket.consumed == 1)
                    {
                        RemoveFood.Invoke(foodPacket.uid);
                    }
                    break;

                case ObjectType.oGuano:
                    GuanoPacket guanoPacket = GuanoPacket.Deserialize(sub.mUpdate);
                    var gameScript = game.GetComponent<IMode>();
                    gameScript.InstantiateGuano(new Vector3(guanoPacket.xPos, guanoPacket.yPos, 0.0f));
                    break;

                case ObjectType.oPipe:
                    PipePacket pipePacket = PipePacket.Deserialize(sub.mUpdate);
                    GameObject pipe = Instantiate(PipePrefab) as GameObject;
                    var pipeScript = pipe.GetComponent<Pipe>();
                    pipeScript.game = this.game;
                    pipe.transform.position = new Vector3(pipePacket.xPos, pipePacket.yPos, 0.0f);
                    pipe.transform.localScale += new Vector3(0.0f, pipePacket.scale, 0);
                    break;

                default:
                    break;
            }
            consumed += 32;
        }
    }
}
