using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Survival : MonoBehaviour, IMode
{
    public GameObject DeathScreenPrefab;
    public GameObject DefeatScreenPrefab;
    public GameObject VictoryScreenPrefab;
    public GameObject Network;
    public GameObject MainCanvas;
    public GameObject StaminaMeter;
    public GameObject GuanoMeter;
    public Text ScoreText;
    public GameObject PlayerPrefab;
    public List<GameObject> pipes = new List<GameObject>();
    public GameObject PipePrefab;
    public GameObject FoodPrefab;
    public GameObject GuanoPrefab;
    public double initialPipeSpeed = 4;
    private GameObject Player;
    private double pipeSpeed;
    private double scoreValue;
    private int foodEaten;
    private int guanoProduced;
    private bool alive;
    private bool started;
    private GameObject endscreen;


    public GameObject GetPlayer() 
    {
        return Player;
    }

    public List<GameObject> GetPipes()
    {
        return pipes;
    }

    public double GetPipeSpeed()
    {
        return pipeSpeed;
    }

    public void Die()
    {
        if (alive)
        {
            alive = false;
            this.Network.GetComponent<UDPClient>().SetGameState(2);
            Network.GetComponent<UDPClient>().changed = true;
            DeathScreen();
        }
    }

    public void NewGame()
    {
        alive = true;
        started = false;
        pipeSpeed = initialPipeSpeed;
        scoreValue = 0;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MultiPlayerMenu");
    }


    // Start is called before the first frame update
    void Start()
    {
        NewGame();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            Network.GetComponent<UDPClient>().SetGameState(2);
            Network.GetComponent<UDPClient>().changed = true;
            Destroy(Player);
            Player = null;
            Die();
        }

        if (alive && (GetStatus() == 1))
        {
            var playerScript = Player.GetComponent<Birb>();
            if ((Keyboard.current != null && Keyboard.current.spaceKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.aButton.wasPressedThisFrame))
            {
                playerScript.Flap();
                Network.GetComponent<UDPClient>().changed = true;
            }
            if ((Keyboard.current != null && Keyboard.current.ctrlKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.xButton.wasPressedThisFrame))
            {
                playerScript.Deficate();
            }

            GuanoMeter.GetComponent<Image>().fillAmount = (float)playerScript.guanoAmount / 3.0f;
            StaminaMeter.GetComponent<Image>().fillAmount = (float)playerScript.stamina / 4.0f;
            pipeSpeed += Time.deltaTime * 0.075;
            scoreValue += Time.deltaTime;
            ScoreText.text = String.Format("Score: {0:000000}", scoreValue);
        }
    }

    public void EatFood(GameObject food)
    {
        foodEaten++;
    }
    public void ProduceGuano(Vector3 guanoPosition)
    {
        Network.GetComponent<UDPClient>().changed = true;
        guanoProduced++;
    }

    public void InstantiateGuano(Vector3 guanoPosition)
    {
        GameObject guano = Instantiate(GuanoPrefab) as GameObject;
        guano.transform.position = guanoPosition;
        guano.GetComponent<Guano>().game = this.gameObject;
    }

    public void ClearInstructions()
    {
        Debug.Log("called ClearInstructions()");
        Player = Instantiate(PlayerPrefab) as GameObject;
        Birb playerScript = Player.GetComponent<Birb>();
        playerScript.game = this.gameObject;
        this.Network.GetComponent<UDPClient>().player = Player;
        started = true;
        this.Network.GetComponent<UDPClient>().SetGameState(1);
        Debug.Log("ClearInstructions() finished");
    }

    public void NetworkEnd(UInt64 uid)
    {
        var my_uid = MultiPlayerStatics.UID;
        if (uid == my_uid)
        {
            VictoryScreen();
        }
        else
        {
            DefeatScreen();
        }
    }

    void VictoryScreen()
    {
        endscreen = Instantiate(VictoryScreenPrefab) as GameObject;
        Text results_values = endscreen.transform.Find("ResultsValues").gameObject.GetComponent<Text>();
        results_values.text = String.Format("{0:000000}\n{1:000000}\n{2:000000}", scoreValue, foodEaten, guanoProduced);
        Button main_menu_button = endscreen.transform.Find("MainMenuButton").gameObject.GetComponent<Button>();
        main_menu_button.onClick.AddListener(this.MainMenu);
        endscreen.transform.SetParent(MainCanvas.transform, false);
    }

    void DefeatScreen()
    {
        endscreen = Instantiate(DefeatScreenPrefab) as GameObject;
        Text results_values = endscreen.transform.Find("ResultsValues").gameObject.GetComponent<Text>();
        results_values.text = String.Format("{0:000000}\n{1:000000}\n{2:000000}", scoreValue, foodEaten, guanoProduced);
        Button main_menu_button = endscreen.transform.Find("MainMenuButton").gameObject.GetComponent<Button>();
        main_menu_button.onClick.AddListener(this.MainMenu);
        endscreen.transform.SetParent(MainCanvas.transform, false);
    }

    void DeathScreen()
    {
        endscreen = Instantiate(DeathScreenPrefab) as GameObject;
        Text results_values = endscreen.transform.Find("ResultsValues").gameObject.GetComponent<Text>();
        results_values.text = String.Format("{0:000000}\n{1:000000}\n{2:000000}", scoreValue, foodEaten, guanoProduced);
        Button observe_button = endscreen.transform.Find("ObserveButton").gameObject.GetComponent<Button>();
        observe_button.onClick.AddListener(this.Observe);
        endscreen.transform.SetParent(MainCanvas.transform, false);
        Button main_menu_button = endscreen.transform.Find("MainMenuButton").gameObject.GetComponent<Button>();
        main_menu_button.onClick.AddListener(this.MainMenu);
        endscreen.transform.SetParent(MainCanvas.transform, false);
    }

    public void Observe()
    {
        Destroy(endscreen);
    }

    public UInt16 GetStatus()
    {
        var netscript = Network.GetComponent<UDPClient>();
        return netscript.GetGameState();
    }
}
