using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    public GameObject game;
    public bool networkOwned = false;
    System.Random random = new System.Random();
    // Start is called before the first frame update
    void Start()
    {
        if (!networkOwned)
        {
            double heightAdjust = random.Next(6000) / 1000.0 - 2.5;
            this.gameObject.transform.position = new Vector3(10.0f, (float)heightAdjust, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate((float)(-1 * game.GetComponent<IMode>().GetPipeSpeed() * Time.deltaTime), 0, 0, Space.World);
        if (this.gameObject.transform.position.x <= -10)
        {
            game.GetComponent<IMode>().GetPipes().Remove(this.gameObject);
            Destroy(this.gameObject);
        }
        else
        {
            Splat();
        }
    }

    void Splat()
    {
        List<ContactPoint2D> contacts = new List<ContactPoint2D>();
        this.gameObject.GetComponent<Collider2D>().GetContacts(contacts);
        if (contacts.Count > 0)
        {
            // Find non-Ceiling contact
            foreach (ContactPoint2D contact in contacts)
            {
                GameObject hitObj = contact.collider.gameObject;
                if (hitObj.tag == "Player")
                {
                    var script = hitObj.GetComponent<Birb>();
                    if (game.GetComponent<IMode>().GetPipeSpeed() > 18.0)
                    {
                        script.Die();
                    }
                }
            }
        }
    }
}
