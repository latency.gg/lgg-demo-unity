using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraShader : MonoBehaviour
{
    [SerializeField]
    private Vector2 cellSize = new Vector2(4, 4);

    private Material material;

    private void Start()
    {
        Screen.SetResolution(1920, 1080, true, 60);
    }

    private void Awake()
    {
        material = new Material(Shader.Find("Hidden/Pixelated"));
    }


    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_ScreenWidth", Screen.width);
        material.SetFloat("_ScreenHeight", Screen.height);
        material.SetFloat("_CellSizeX", cellSize.x);
        material.SetFloat("_CellSizeY", cellSize.y);
        Graphics.Blit(source, destination, material);
    }
}
