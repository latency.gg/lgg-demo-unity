using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    public GameObject game;
    public Rigidbody2D rb;
    public static Dictionary<UInt64,Enemy> Enemies = new Dictionary<UInt64, Enemy>();

    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (game.GetComponent<IMode>().GetStatus() == 0)
        {
            rb.Sleep();
        }
        else if ((game.GetComponent<IMode>().GetStatus() == 0) && rb.IsSleeping())
        {
            rb.WakeUp();
        }
    }

    public void Die()
    {
        game.GetComponent<IMode>().Die();
        Destroy(this.gameObject);
    }

    public static void VectorUpdate(UInt64 uid, Vector2 pos, Vector2 vel)
    {
        Enemy enemy;
        Enemies.TryGetValue(uid, out enemy);
        if (!(enemy is null))
        {
            enemy.VectorUpdateIndividual(pos, vel);
        }
    }

    public void VectorUpdateIndividual(Vector2 pos, Vector2 vel)
    {
        this.gameObject.transform.position = pos;
        this.rb.velocity = vel;
    }

    public void Hit()
    {
        rb.velocity = new Vector2(rb.velocity.x - 0.1f, rb.velocity.y - 0.1f);
    }
}
