using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreferencesMenu : MonoBehaviour
{
    public GameObject DisplayNamePlaceholder;
    public GameObject DisplayNameInput;
    public string playerURL = "http://127.0.0.1:5000/";
    // Start is called before the first frame update
    void Start()
    {
        string UID = PlayerPrefs.GetString("sUID", "");
        if (UID != "")
        {
            PreferencesAPI apiResponse;
            try
            {
                using (WebClient client = new WebClient())
                {
                    string credentials = Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(PlayerPrefs.GetString("sUID") + ":" + PlayerPrefs.GetString("password")));
                    client.Headers[HttpRequestHeader.Authorization] = string.Format(
                        "Basic {0}", credentials);
                    string response = client.DownloadString(new Uri(playerURL + "player/" + UID));
                    apiResponse = JsonConvert.DeserializeObject<PreferencesAPI>(response);
                }
            }
            catch (WebException e)
            {
                HttpWebResponse response = (HttpWebResponse)e.Response;
                if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    string display_name = PlayerPrefs.GetString("displayName");
                    Register(display_name);
                    if (MultiPlayerStatics.SelectedScene != "PreferencesMenu")
                    {
                        SceneManager.LoadScene("MatchMaking");
                    }
                }
            }
            string displayname = PlayerPrefs.GetString("displayName");
            var text = DisplayNamePlaceholder.GetComponent<TextMeshProUGUI>();
            if (displayname != "")
            {
                text.text = displayname;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Save() 
    {
        var text = DisplayNameInput.GetComponent<TextMeshProUGUI>();
        string UID = PlayerPrefs.GetString("sUID", "");
        if (UID != "")
        {
            SetDisplayName(text.text);
        } else {
            Register(text.text);
        }
            SceneManager.LoadScene("MultiPlayerMenu");
    }

    private void SetDisplayName(string display_name)
    {
        string UID = PlayerPrefs.GetString("sUID");
        Debug.Log(UID);
        PreferencesAPI apiResponse;
        try
        {
            using (WebClient client = new WebClient())
            {
                string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(PlayerPrefs.GetString("sUID") + ":" + PlayerPrefs.GetString("password")));
                client.Headers[HttpRequestHeader.Authorization] = string.Format(
                    "Basic {0}", credentials);
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var preferencesAPI = new PreferencesAPI
                {
                    uid = Convert.ToUInt64(UID),
                    display_name = display_name
                };
                var dataString = JsonConvert.SerializeObject(preferencesAPI);
                string response = client.UploadString(new Uri(playerURL + "player/" + UID), "PUT", dataString);
                Debug.Log(response);
                apiResponse = JsonConvert.DeserializeObject<PreferencesAPI>(response);
                PlayerPrefs.SetString("displayName", apiResponse.display_name);
            }
        }
        catch (WebException e)
        {
            HttpWebResponse response = (HttpWebResponse)e.Response;
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                SceneManager.LoadScene("PreferencesMenu");
            }
            else
            {
                Debug.Log("Error contacting API" + e.Message);
                SceneManager.LoadScene("Menu");
            }
        }
    }

    private void Register(string display_name)
    {
        RegisterAPI apiResponse;
        string uid = "";
        try
        {
            using (WebClient client = new WebClient())
            {
                var preferencesAPI = new PreferencesAPI
                {
                    uid = 0,
                    display_name = display_name
                };
                var dataString = JsonConvert.SerializeObject(preferencesAPI);
                string response = client.UploadString(new Uri(playerURL + "player"), "POST", dataString);
                apiResponse = JsonConvert.DeserializeObject<RegisterAPI>(response);
                uid = string.Format("{0}", apiResponse.uid);
                PlayerPrefs.SetString("sUID", uid);
                PlayerPrefs.SetString("displayName", apiResponse.display_name);
                PlayerPrefs.SetString("password", apiResponse.password);
            }

        }
        catch (WebException e)
        {
            HttpWebResponse response = (HttpWebResponse)e.Response;
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                SceneManager.LoadScene("PreferencesMenu");
            }
            else
            {
                Debug.Log("Error contacting API" + e.Message);
                SceneManager.LoadScene("Menu");
            }
        }
    }

    public void Abort()
    {
        SceneManager.LoadScene("MultiPlayerMenu");
    }
}


[System.Serializable]
public class RegisterAPI
{
    public UInt64 uid;
    public string display_name;
    public string password;
}

[System.Serializable]
public class PreferencesAPI
{
    public UInt64 uid;
    public string display_name;
    public string top_score;
}