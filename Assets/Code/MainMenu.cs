using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    void Update()
    {
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            QuitGame();
        }
    }

    public void SinglePlayerGame()
    {
        SceneManager.LoadScene("SinglePlayerScene");
    }

    public void MultiPlayerGame()
    {
        SceneManager.LoadScene("MultiPlayerMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
