using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SinglePlayer : MonoBehaviour, IMode
{
    public GameObject EndScreenPrefab;
    public GameObject MainCanvas;
    public GameObject StaminaMeter;
    public GameObject GuanoMeter;
    public Text ScoreText;
    public GameObject PlayerPrefab;
    public List<GameObject> pipes = new List<GameObject>();
    public GameObject PipePrefab;
    public GameObject FoodPrefab;
    public GameObject GuanoPrefab;
    public double initialGenerationFrequency = 0.25;
    public double initialPipeSpeed = 4;
    private GameObject Player;
    private double pipeSpeed;
    private double generationFrequency;
    private double scoreValue;
    private int foodEaten;
    private int guanoProduced;
    private double timeSinceGeneration;
    private bool alive;
    private UInt16 status = 0;
    private GameObject endscreen;
    System.Random random = new System.Random();


    public GameObject GetPlayer() 
    {
        return Player;
    }

    public List<GameObject> GetPipes()
    {
        return pipes;
    }

    public double GetPipeSpeed()
    {
        return pipeSpeed;
    }

    public void Die()
    {
        if (alive)
        {
            alive = false;
            EndScreen();
        }
    }

    public void NewGame()
    {
        scoreValue = 0;
        timeSinceGeneration = 0;
        generationFrequency = initialGenerationFrequency;
        pipeSpeed = initialPipeSpeed;
        Player = Instantiate(PlayerPrefab) as GameObject;
        Player.GetComponent<Birb>().game = this.gameObject;
        alive = true;
        if (!(endscreen is null))
        {
            Destroy(endscreen);
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }


    // Start is called before the first frame update
    void Start()
    {
        NewGame();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            Destroy(Player);
            Player = null;
            Die();
        }
        if (alive && (status == 0))
        {
            if ((Keyboard.current != null && Keyboard.current.spaceKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.aButton.wasPressedThisFrame))
            {
                status = 1;
                var playerScript = Player.GetComponent<Birb>();
                playerScript.Flap();
                ClearInstructions();
            }
        }
        else if (alive && (status != 0))
        {
            var playerScript = Player.GetComponent<Birb>();
            if ((Keyboard.current != null && Keyboard.current.spaceKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.aButton.wasPressedThisFrame))
            {
                playerScript.Flap();
            }
            if ((Keyboard.current != null && Keyboard.current.ctrlKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.xButton.wasPressedThisFrame))
            {
                playerScript.Deficate();
            }

            CreatePipe();
            CreateFood();
            if (scoreValue > 500)
            {
                CreateGuano();
            }

            GuanoMeter.GetComponent<Image>().fillAmount = (float)playerScript.guanoAmount / 3.0f;
            StaminaMeter.GetComponent<Image>().fillAmount = (float)playerScript.stamina / 4.0f;
            pipeSpeed += Time.deltaTime * 0.075;
            generationFrequency += Time.deltaTime * 0.01;
            scoreValue += Time.deltaTime;
            ScoreText.text = String.Format("Score: {0:000000}", scoreValue);
        }
    }

    public void EatFood(GameObject food)
    {
        foodEaten++;
    }
    public void ProduceGuano(Vector3 guanoPosition)
    {
        guanoProduced++;
        InstantiateGuano(guanoPosition);
    }

    public void InstantiateGuano(Vector3 guanoPosition)
    {
        GameObject guano = Instantiate(GuanoPrefab) as GameObject;
        guano.transform.position = guanoPosition;
        guano.GetComponent<Guano>().game = this.gameObject;
    }

    public UInt16 GetStatus()
    {
        return status;
    }

    public void ClearInstructions()
    {

    }

    public void NetworkEnd(UInt64 uid){}

    public void Observe() {}

    void EndScreen()
    {
        endscreen = Instantiate(EndScreenPrefab) as GameObject;
        GameObject highscore_alert = endscreen.transform.Find("HighscoreAlert").gameObject;
        if (true) // If new highscore don't do this;
        {
            Destroy(highscore_alert);
        }
        Text results_values = endscreen.transform.Find("ResultsValues").gameObject.GetComponent<Text>();
        results_values.text = String.Format("{0:000000}\n{1:000000}\n{2:000000}", scoreValue, foodEaten, guanoProduced);
        Button new_game_button = endscreen.transform.Find("NewGameButton").gameObject.GetComponent<Button>();
        new_game_button.onClick.AddListener(this.NewGame);
        endscreen.transform.SetParent(MainCanvas.transform, false);
        Button main_menu_button = endscreen.transform.Find("MainMenuButton").gameObject.GetComponent<Button>();
        main_menu_button.onClick.AddListener(this.MainMenu);
        endscreen.transform.SetParent(MainCanvas.transform, false);
    }

    void CreatePipe()
    {
        GameObject? previousPipe = null;
        if (pipes.Count > 1)
        {
            previousPipe = pipes[pipes.Count - 1];
        }
        if ((previousPipe is null) || (previousPipe.transform.position.x < 9))
        {
            if (timeSinceGeneration >= 1 / generationFrequency)
            {
                GameObject pipe = Instantiate(PipePrefab) as GameObject;
                pipes.Add(pipe);
                pipe.GetComponent<Pipe>().game = this.gameObject;
                timeSinceGeneration = 0;
            }
            else
            {
                timeSinceGeneration += Time.deltaTime;
            }
        }
    }

    void CreateFood()
    {
        if (random.NextDouble() > 0.9995)
        {
            GameObject food = Instantiate(FoodPrefab) as GameObject;
            food.GetComponent<Food>().game = this.gameObject;
        }
    }

    void CreateGuano()
    {
        if (random.NextDouble() > 0.998)
        {
            GameObject guano = Instantiate(GuanoPrefab) as GameObject;
            guano.transform.position = new Vector3(Player.transform.position.x + 1.0f, Player.transform.position.y + 0.8f, 0.0f);
            guano.GetComponent<Guano>().game = this.gameObject;
        }
    }
}
