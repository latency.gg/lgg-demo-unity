using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;
using Newtonsoft.Json;
using LatencyGG;

public class latencymeasurement : MonoBehaviour
{

    Thread latencyGGThread;
    bool started = false;
    double timeSince = 0.0;

    class LatencyWorker
    {
        // The code actually running on the job
        public void Execute()
        {
            while (true)
            {
                try
                {
                    Tuple<LatencyGGGameAPI, string, string> metrics = FetchBeacons();
                    LatencyGG.LatencyGG latency = initialiseLatency(metrics);
                    latency.Run();
                } catch(WebException) 
                {
                    Debug.Log("Error contacting API");
                }
                Thread.Sleep(1000);
            }
        }

        private Tuple<LatencyGGGameAPI, string, string> FetchBeacons()
        {
            string url = "https://demo.latency.gg/metrics";
            LatencyGGGameAPI apiResponse;
            string ident;
            string addr;
            using (WebClient client = new WebClient())
            {
                string response = client.DownloadString(new Uri(url));
                apiResponse = JsonConvert.DeserializeObject<LatencyGGGameAPI>(response);
                ident = apiResponse.source.ident;
                addr = apiResponse.source.addr;
            }
            return new Tuple<LatencyGGGameAPI, string, string>(apiResponse, ident, addr);
        }

        private LatencyGG.LatencyGG initialiseLatency(Tuple<LatencyGGGameAPI, string, string> metrics)
        {
            LatencyGGGameAPI apiResponse = metrics.Item1;
            string ident = metrics.Item2;
            string addr = metrics.Item3;
            LatencyGG.AF af;
            bool ipv6 = (apiResponse.source.version == 6);
            if (ipv6)
            {
                af = LatencyGG.AF.eINET6;
            }
            else
            {
                af = LatencyGG.AF.eINET;
            }
            LatencyGG.LatencyGG latency = new LatencyGG.LatencyGG(af, 3000);
            var random = new System.Random((int)Convert.ToUInt64(DateTimeOffset.Now.UtcDateTime.Ticks / 10000L - 62135596800000L));
            foreach ((String provider_name, var locations) in apiResponse.stale_metrics)
            {
                foreach ((String location_name, LatencyGGGameMetrics metric) in locations)
                {
                    if (metric.beacons.Count != 0)
                    {
                        int index = random.Next(metric.beacons.Count);
                        LatencyGGGameBeacon selected = metric.beacons[index];
                        int dpversion = 3;
                        String beacon_ip;
                        if (ipv6)
                        {
                            beacon_ip = selected.ipv6;
                        }
                        else
                        {
                            beacon_ip = selected.ipv4;
                        }
                        LatencyGG.DataPing dataping = new LatencyGG.DataPing(beacon_ip, addr, ident, selected.token, dpversion);
                        latency.Add(dataping);
                    }
                }
            }
            return latency;
        }


    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("LatencyMeasurement");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        Initialize();
    }

    void Initialize()
    {
        LatencyWorker worker = new LatencyWorker();
        latencyGGThread = new Thread(new ThreadStart(worker.Execute));
        latencyGGThread.IsBackground = true;
        latencyGGThread.Start();
    }

    // Update is called once per frame
    void Update()
    {
    }
}

static class KvpExtensions
{
    public static void Deconstruct<TKey, TValue>(
        this KeyValuePair<TKey, TValue> kvp,
        out TKey key,
        out TValue value)
    {
        key = kvp.Key;
        value = kvp.Value;
    }
}

[System.Serializable]
public class LatencyGGGameAPI
{
    public LatencyGGGameSource source;
    public Dictionary<String, Dictionary<String, LatencyGGGameMetrics>> stale_metrics;
}

[System.Serializable]
public class LatencyGGGameSource
{
    public String addr;
    public int version;
    public String ident;
}

[System.Serializable]
public class LatencyGGGameMetrics
{
    public List<LatencyGGGameBeacon> beacons;
    public double rtt;
    public double stddev;
}

[System.Serializable]
public class LatencyGGGameBeacon
{
    public String ipv4;
    public String ipv6;
    public String token;
}