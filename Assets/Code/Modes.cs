using System;
using System.Collections.Generic;
using UnityEngine;

interface IMode
{
    public GameObject GetPlayer();
    public List<GameObject> GetPipes();
    public double GetPipeSpeed();
    public void Die();
    public void NewGame();
    public void Observe();
    public void NetworkEnd(UInt64 uid);
    public UInt16 GetStatus();
    public void ClearInstructions();
    public void EatFood(GameObject food);
    public void ProduceGuano(Vector3 guanoPosition);
    public void InstantiateGuano(Vector3 guanoPosition);
}
