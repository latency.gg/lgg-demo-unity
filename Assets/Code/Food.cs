using System;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public GameObject game;
    public static Dictionary<UInt32, GameObject> Foods = new Dictionary<UInt32, GameObject>() { };
    public bool network = false;
    public UInt32 uid;
    System.Random random = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        if (!network)
        {
            double heightAdjust = random.Next(80) / 10.0 - 3.55;
            this.gameObject.transform.position = new Vector3(10, (float)heightAdjust, 0);
        }
        else
        {
            Debug.Log(uid);
            Foods[uid] = this.gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate((float)(-1 * game.GetComponent<IMode>().GetPipeSpeed() * Time.deltaTime), 0, 0, Space.World);
        if (this.gameObject.transform.position.x <= -10)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Eaten();
        }
    }

    public static void Remove(UInt32 fooduid)
    {
        GameObject food;
        Foods.TryGetValue(fooduid, out food);
        if (!(food is null))
        {
            Destroy(food);
            Foods.Remove(fooduid);
        }
    }

    void Eaten()
    {
        List<ContactPoint2D> contacts = new List<ContactPoint2D>();
        this.gameObject.GetComponent<Collider2D>().GetContacts(contacts);
        if (contacts.Count > 0)
        {
            // Find non-Ceiling contact
            foreach (ContactPoint2D contact in contacts)
            {
                GameObject hitObj = contact.collider.gameObject;
                if (hitObj.tag == "Player")
                {
                    var script = hitObj.GetComponent<Birb>();
                    if (script.guanoAmount < 3)
                    {
                        script.EatFood(this.gameObject);
                        Destroy(this.gameObject);
                        Foods.Remove(this.uid);
                    }
                }
            }
        }
        
    }
}
