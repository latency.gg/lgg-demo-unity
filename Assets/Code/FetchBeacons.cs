using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;
using Newtonsoft.Json;
using LatencyGG;


public class FetchBeacons : MonoBehaviour
{
    public GameObject beaconPrefab;
    public GameObject linePrefab;
    public GameObject probe;
    public Text outputTags;
    public Text outputRtts;
    public Text outputPingType;
    static Mutex initLock = new Mutex();
    bool initialising = false;
    bool running = false;
    GameObject circle;

    LatencyGG.LatencyGG latency = new LatencyGG.LatencyGG();
    Dictionary<String, List<String>> lookupIP = new Dictionary<String, List<String>>();
    Dictionary<String, String> lookupIPInverse = new Dictionary<String, String>();
    static Mutex squareLock = new Mutex();
    Dictionary<String, GameObject> lookupSquare = new Dictionary<String, GameObject>();
    Dictionary<String, GameObject> lookupLines = new Dictionary<String, GameObject>();
    Dictionary<String, Double> lookupAngle = new Dictionary<String, Double>();
    Dictionary<String, Double> lookupNewAngle = new Dictionary<String, Double>();
    Dictionary<String, Double> lookupNewDistance = new Dictionary<String, Double>();
    Dictionary<String, Double> lookupRTT = new Dictionary<String, Double>();
    List<Tuple<Double, String>> readyLocations = new List<Tuple<Double, String>>();
    LatencyGGDemoAPI apiResponse;
    String ident; 
    String addr;
    Double maxLatency = 500;
    Double probeScale = 1;
    Double probeAnimationTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        circle = GameObject.Find("Probe");
        running = true;
    }

    async void Update()
    {
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            CloseButton();
        }
        if (!initialising && running)
        {
            if (latency.IsComplete())
            {
                updateBeacons();
                updateLine();
            }
            updateLists();
            updateBeaconLocations();
            if (!latency.IsRunnable())
            {
                await RunMeasurement();
            }
        }
    }

    public void CloseButton()
    {
        latency.Kill();
        running = false;
        while (latency.IsRunnable())
        {
            Debug.Log("Waiting");
        }
        SceneManager.LoadScene("Menu");
    }

    private async Task RunMeasurement()
    {
        if (initLock.WaitOne(1))
        {
            Debug.Log("Preparing measurement");
            initialising = true;
            await callAPI();
            UInt16 flat_count = initialiseLatency();
            spawnBeacons(flat_count);
            initialising = false;
            Debug.Log("Starting measurement");
            await Task.Run(latency.Run);
            initLock.ReleaseMutex();
        }
    }



    private async Task callAPI()
    {
        String url = "https://demo.latency.gg/metrics";

        using (WebClient client = new WebClient())
        {
            client.DownloadStringCompleted += (sender, evnt) =>
            {
                String response = evnt.Result;
                apiResponse = JsonConvert.DeserializeObject<LatencyGGDemoAPI>(response);
                ident = apiResponse.source.ident;
                addr = apiResponse.source.addr;
            };
            await client.DownloadStringTaskAsync(new Uri(url));
        }
    }

    private UInt16 initialiseLatency()
    {
        LatencyGG.AF af;
        bool ipv6 = (apiResponse.source.version == 6);
        if (ipv6)
        {
            af = LatencyGG.AF.eINET6;
        } else
        {
            af = LatencyGG.AF.eINET;
        }
        latency = new LatencyGG.LatencyGG(af, 3000);
        lookupIP = new Dictionary<String, List<String>>();
        lookupIPInverse = new Dictionary<String, String>();
        readyLocations = new List<Tuple<Double, String>>();
        outputPingType.text = "Ping Type: DataPing";
        var random = new System.Random();
        UInt16 flat_count = 0;
        foreach ((String provider_name, var locations) in apiResponse.stale_metrics)
        {
            foreach ((String location_name, LatencyGGDemoMetrics metric) in locations)
            {
                if (metric.beacons.Count != 0)
                {
                    int index = random.Next(metric.beacons.Count);
                    var selected = metric.beacons[index];
                    int dpversion = 3;
                    String beacon_ip;
                    if (ipv6)
                    {
                        beacon_ip = selected.ipv6;
                    }
                    else
                    {
                        beacon_ip = selected.ipv4;
                    }
                    LatencyGG.DataPing dataping = new LatencyGG.DataPing(beacon_ip, addr, ident, selected.token, dpversion);
                    latency.Add(dataping);
                    lookupIP[beacon_ip] = new List<String>();
                    lookupIP[beacon_ip].Add(provider_name);
                    lookupIP[beacon_ip].Add(location_name);
                    String tags = getTags(beacon_ip);
                    lookupIPInverse[tags] = beacon_ip;
                    flat_count++;
                }
            }
        }
        foreach ((String provider_name, var locations) in apiResponse.clean_metrics)
        {
            foreach ((String location_name, LatencyGGDemoMetrics metric) in locations)
            {
                String tags = provider_name + "," + location_name;
                Double rtt;
                if (lookupRTT.ContainsKey(tags))
                {
                    rtt = lookupRTT[tags];
                }
                else
                {
                    rtt = metric.rtt;
                }
                var result = new Tuple<Double, String>(rtt, tags);
                readyLocations.Add(result);
                if (metric.beacons.Count != 0)
                {
                    int index = random.Next(metric.beacons.Count);
                    var selected = metric.beacons[index];
                    int dpversion = 3;
                    String beacon_ip;
                    if (ipv6)
                    {
                        beacon_ip = selected.ipv6;
                    }
                    else
                    {
                        beacon_ip = selected.ipv4;
                    }
                    LatencyGG.DataPing dataping = new LatencyGG.DataPing(beacon_ip, addr, ident, selected.token, dpversion);
                    latency.Add(dataping);
                    lookupIP[beacon_ip] = new List<String>();
                    lookupIP[beacon_ip].Add(provider_name);
                    lookupIP[beacon_ip].Add(location_name);
                    lookupIPInverse[tags] = beacon_ip;
                    flat_count++;
                }
            }
        }
        return flat_count;
    }

    private String getTags(String aBeaconIp)
    {
        var tags_list = lookupIP[aBeaconIp];
        String tags = tags_list[0] + "," + tags_list[1];
        return tags;
    }

    private void spawnBeacons(UInt16 aFlatCount)
    {
        UInt16 i = 0;
        foreach (String beacon_ip in lookupIP.Keys)
        {
            String tags = getTags(beacon_ip);
            squareLock.WaitOne();
            if (!lookupSquare.ContainsKey(tags))
            {
                spawnBeacon(3.5, (i+1) * (360 / (aFlatCount)), beacon_ip, tags);
                spawnLine(tags);
            }
            else
            {   
                Double bearing = (i+1) * (360 / (aFlatCount));
                Double radians = bearing * (Math.PI / 180);
                lookupNewAngle[tags] = radians;
            }
            squareLock.ReleaseMutex();
            i++;
        }
    }

    private void spawnBeacon(double aDistance, double aBearing, String aBeaconIp, String aTags)
    {
        GameObject square = Instantiate(beaconPrefab) as GameObject;
        double radians = aBearing * (Math.PI / 180);
        double x = aDistance * Math.Sin(radians);
        double y = aDistance * Math.Cos(radians);
        square.transform.Translate(new Vector3((float)x, (float)y, (float)0), Space.Self);
        GameObject textobj = square.gameObject.transform.GetChild(0).gameObject;
        TextMeshPro t = textobj.GetComponent<TextMeshPro>();
        t.text = lookupIP[aBeaconIp][0] + "\n" + lookupIP[aBeaconIp][1];
        lookupAngle[aTags] = radians;
        squareLock.WaitOne();
        lookupSquare[aTags] = square;
        squareLock.ReleaseMutex();
    }

    private void spawnLine(String aTags)
    {
        GameObject panel = GameObject.Find("LineGraphPanel");
        Material lineMat = new Material(Shader.Find("Unlit/Color"));
        Color lineColor = new Color(0.9059f, 0.7176f, 0.3961f, 1.0f);
        lineMat.SetColor("_Color", lineColor);
        GameObject line = Instantiate(linePrefab) as GameObject;
        line.transform.SetParent(panel.transform);
        line.layer = 7;
        LineRenderer lr = line.GetComponent<LineRenderer>();
        lookupLines[aTags] = line;
        lr.material = lineMat;
        double z = 1;
        double y = -3;
        double x = -8;
        lr.positionCount = 21;
        for (int i = 0; i < 21; i++)
        {
            lr.SetPosition(i, new Vector3((float)x, (float)y, (float)z));
            x += 0.8;
        }
        lr.startWidth = .01f;
        lr.endWidth = .012f;
    }

    private void updateBeaconLocations()
    {
        squareLock.WaitOne();
        foreach (string tags in lookupSquare.Keys)
        {
            if (lookupAngle.ContainsKey(tags))
            {
                GameObject square = lookupSquare[tags];
                double angle_radians = lookupAngle[tags];
                if (lookupNewAngle.ContainsKey(tags) && (lookupNewAngle[tags] < angle_radians || lookupNewAngle[tags] > angle_radians))
                {
                    angle_radians = (lookupNewAngle[tags] + angle_radians*29) / 30;
                    if (Math.Abs(lookupNewAngle[tags] - angle_radians) < (2 * (Math.PI / 180))) // if diff is < 2 degrees
                    {
                        angle_radians = lookupNewAngle[tags];
                        lookupNewAngle.Remove(tags);
                    }
                    lookupAngle[tags] = angle_radians;
                }
                double old_distance = Math.Sqrt(Math.Pow(square.transform.localPosition[0], 2) + Math.Pow(square.transform.localPosition[1], 2));
                double target_distance = old_distance;
                if (lookupNewDistance.ContainsKey(tags))
                {
                    target_distance = lookupNewDistance[tags];
                }
                double new_distance = (old_distance*59 + target_distance) / 60;
                if (Math.Abs(new_distance-target_distance) < 0.01)
                {
                    new_distance = target_distance;
                    lookupNewDistance.Remove(tags);
                }
                double x = new_distance * Math.Sin(angle_radians);
                double y = new_distance * Math.Cos(angle_radians);
                square.transform.localPosition = new Vector3((float)x, (float)y, (float)0);
                GameObject textobj = square.gameObject.transform.GetChild(0).gameObject;
                TextMeshPro t = textobj.GetComponent<TextMeshPro>();
                if (lookupRTT.ContainsKey(tags))
                {
                    t.text = String.Format("{0:0}", lookupRTT[tags]) + "ms";
                }
            }
        }
        squareLock.ReleaseMutex();
    }
    

    private double findMaxLatency()
    {
        double newMaxLatency = 0;
        foreach (String ip in latency.GetKeys())
        {
            double rtt = latency[ip].GetStats().mRtt;
            newMaxLatency = Math.Max(newMaxLatency, rtt);
        }
        return newMaxLatency;
    }

    private void updateLists()
    {
        outputTags.text = "\nReady locations: \n\n";
        outputRtts.text = "\n\n\n";
        readyLocations.Sort();
        foreach ((Double rtt, String tags) in readyLocations)
        {
            outputTags.text = outputTags.text + tags + "\n";
            outputRtts.text = outputRtts.text + rtt + "ms" + "\n";
        }
    }

    private void updateLine()
    {
        foreach (String tags in lookupRTT.Keys)
        {
            if (!lookupLines.ContainsKey(tags))
            {
                continue;
            }
            GameObject line = lookupLines[tags];
            LineRenderer lr = line.GetComponent<LineRenderer>();
            var nextPosition = lr.GetPosition(0);
            var currentPosition = lr.GetPosition(0);
            for (int i = 20; i > 0; i--)
            {
                nextPosition = lr.GetPosition(i - 1);
                currentPosition = lr.GetPosition(i);
                lr.SetPosition(i, new Vector3((float)currentPosition.x, (float)nextPosition.y, (float)currentPosition.z));
            }
            currentPosition = lr.GetPosition(0);
            Double y = (lookupRTT[tags]/250) * (-3 + 4.8) + -4.8;
            lr.SetPosition(0, new Vector3((float)currentPosition.x, (float)y, (float)currentPosition.z));
        }
    }

    private void updateBeacons()
    {
     
        maxLatency = (maxLatency + findMaxLatency()*3)/4;
        foreach (String ip in latency.GetKeys()) {
            double rtt = latency[ip].GetStats().mRtt;
            String tags = getTags(ip);
            double distance = (rtt / maxLatency * 3) + 0.5;
            squareLock.WaitOne();
            if (lookupSquare.ContainsKey(tags))
            {
                lookupNewDistance[tags] = distance;
                lookupRTT[tags] = rtt;
            }
            squareLock.ReleaseMutex();
        }
        
    }

}


static class KvpExtensions
{
    public static void Deconstruct<TKey, TValue>(
        this KeyValuePair<TKey, TValue> kvp,
        out TKey key,
        out TValue value)
    {
        key = kvp.Key;
        value = kvp.Value;
    }
}

[System.Serializable]
public class LatencyGGDemoAPI
{
    public LatencyGGDemoSource source;
    public Dictionary<String, Dictionary<String, LatencyGGDemoMetrics>> clean_metrics;
    public Dictionary<String, Dictionary<String, LatencyGGDemoMetrics>> stale_metrics;
}

[System.Serializable]
public class LatencyGGDemoSource
{
    public String addr;
    public int version;
    public String ident;
}

[System.Serializable]
public class LatencyGGDemoMetrics
{
    public List<LatencyGGDemoBeacon> beacons;
    public double rtt;
    public double stddev;
    public bool stale;
}

[System.Serializable]
public class LatencyGGDemoBeacon
{
    public String ipv4;
    public String ipv6;
    public String token;
}


[System.Serializable]
public class GeoIPNetworkAPI
{
    public String prefix;
    public String region;
    public String allocated_cc;
    public String asn;
    public String rir;
    [JsonProperty(PropertyName = "as-name")]
    public String as_name;
    public GeoJSON geo;
    public Double radius;
    public String timestamp;
}

[System.Serializable]
public class GeoJSON
{
    public String type;
    public GeoJSONProperties properties;
    public GeoJSONGeometry geometry;
}

[System.Serializable]
public class GeoJSONGeometry
{
    public String type;
    public List<Double> coordinates;
}

[System.Serializable]
public class GeoJSONProperties
{
    public Double radius;
}