using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxContainer : MonoBehaviour
{
    public GameObject[] layer0;
    public float layer0Speed = 1.0f;
    public Vector3 layer0Init = new Vector3(18, 0, 0);
    public Vector2 layer0Direction = new Vector2(-1, 0);
    public GameObject[] layer1;
    public float layer1Speed = 5.0f;
    public Vector3 layer1Init = new Vector3(18, 0, 0);
    public Vector2 layer1Direction = new Vector2(-1, 0);
    public GameObject[] layer2;
    public float layer2Speed = 7.0f;
    public Vector3 layer2Init = new Vector3(18, 0, 0);
    public Vector2 layer2Direction = new Vector2(-1, 0);
    public GameObject[] layer3;
    public float layer3Speed = 8.0f;
    public Vector3 layer3Init = new Vector3(18, 0, 0);
    public Vector2 layer3Direction = new Vector2(-1, 0);
    public GameObject[] layer4;
    public float layer4Speed = 10.0f;
    public Vector3 layer4Init = new Vector3(18, 0, 0);
    public Vector2 layer4Direction = new Vector2(-1, 0);
    public float offscreen = 18.0f;
    public GameObject Game;
    private Vector2[] direction;
    private GameObject[][] layers;
    private float[] speed;
    private Vector3[] initialPosition;
    private IMode mode;

    // Start is called before the first frame update
    void Start()
    {
        mode = Game.GetComponent<IMode>();
        layers = new GameObject[5][] { layer0, layer1, layer2, layer3, layer4 };
        speed = new float[] { layer0Speed, layer1Speed, layer2Speed, layer3Speed, layer4Speed };
        initialPosition = new Vector3[] { layer0Init, layer1Init, layer2Init, layer3Init, layer4Init };
        direction = new Vector2[] { layer0Direction, layer1Direction, layer2Direction, layer3Direction, layer4Direction };
    }

    // Update is called once per frame
    void Update()
    {
        
        float pipeSpeed = (float)mode.GetPipeSpeed();
        float deltaT = Time.deltaTime;
        for (int i = 0; i < 5; i++) {
            Vector3 movement = new Vector3(pipeSpeed * speed[i] * direction[i].x, pipeSpeed * speed[i] * direction[i].y, 0);
            movement *= deltaT;
            if (layers[i].Length < 1)
            {
                continue;
            }
            GameObject obj = layers[i][0];
            obj.transform.Translate(movement);
            if (obj.transform.position.x <= -(offscreen))
            {
                var new_x = initialPosition[i].x + (obj.transform.position.x+offscreen);
                var new_y = initialPosition[i].y;
                obj.transform.position = new Vector3(new_x, new_y, 0);
            }
            
        }
    }
}
