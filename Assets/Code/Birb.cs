using System;
using System.Collections.Generic;
using UnityEngine;


public class Birb : MonoBehaviour
{
    public GameObject GuanoPrefab;
    public GameObject game;
    public float distance;
    public double stamina;
    public double staminaRecovery;
    public float glideSpeed;
    public UInt32 eaten;
    public bool hasEaten;
    public Rigidbody2D rb;
    public bool deficated = false;
    private bool gliding = false;
    public int guanoAmount = 0;
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        animator = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per Physics frame
    void FixedUpdate()
    {
        if (game.GetComponent<IMode>().GetStatus() == 0)
        {
            rb.Sleep();
        }
        else if ((game.GetComponent<IMode>().GetStatus() == 0) && rb.IsSleeping())
        {
            rb.WakeUp();
        }
        CheckDead();
        CalculateGlide();
        CalculateStamina();
        animUpdate();
    }

    private void animUpdate()
    {
        animator.SetFloat("TimeSince", animator.GetFloat("TimeSince") + Time.deltaTime);
        float rot = 0.0f;
        if (rb.velocity.y > 0.0f)
        {
            float scaledVelocity = (rb.velocity.y) / (5.0f) * (-5.0f - 4.0f) + 4.0f; // ((val - lowIn) / (highIn-lowIn)) * (highOut-lowOut) + lowOut
            scaledVelocity = (float)Math.Atan(scaledVelocity);
            rot = (scaledVelocity - (-1.5f)) / (1.5f - (-1.5f)) * (0.0f - 45.0f) + 45.0f;
        } else
        {
            float scaledVelocity = (rb.velocity.y - (-10)) / (0.0f - (-10)) * (3.5f - (-5.0f)) + (-5.0f); // ((val - lowIn) / (highIn-lowIn)) * (highOut-lowOut) + lowOut
            scaledVelocity = (-1.0f)*(float)Math.Atan(scaledVelocity);
            rot = (scaledVelocity - (-1.5f)) / (1.5f - (-1.5f)) * (-60.0f - 5.0f) + 5.0f;
        }

        rb.rotation = rot;
    }

    void CheckDead()
    {
        if (this.gameObject.transform.position.x <= -9.6)
        {
            Die();
        }
    }
    public void Die()
    {
        game.GetComponent<IMode>().Die();
        //Destroy(this.gameObject);
    }

    void CalculateGlide()
    {
        if ((this.gameObject.transform.position.x < 0.0f) && (rb.velocity.y < distance))
        {
            rb.velocity = new Vector2(glideSpeed * (Math.Abs(rb.velocity.y)), rb.velocity.y);
            gliding = true;
        }
        if (this.gameObject.transform.position.x >= 0.0 && (rb.velocity.y < distance))
        {
            if (this.gameObject.transform.position.x > 0.5f)
            {
                rb.velocity = new Vector2(rb.velocity.x - 0.1f, rb.velocity.y);
            }
            if ( (0.0 < this.gameObject.transform.position.x) && (this.gameObject.transform.position.x < 0.5) )
            {
                rb.velocity = new Vector2(rb.velocity.x/2.0f, rb.velocity.y);

                if (Math.Abs(rb.velocity.x) < 0.001f)
                {
                    rb.velocity = new Vector2(0.0f, rb.velocity.y);
                }
            }
            else
            {
                gliding = false;
            }
        }
    }

    void CalculateStamina()
    {
        if (stamina <= 4)
        {
            var heightBoost = 2/(this.gameObject.transform.position.y + 3.5);
            if (gliding)
            {
                stamina += (0.7 * (heightBoost * staminaRecovery) * Time.deltaTime);
            }
            else
            {
                stamina += ((heightBoost * staminaRecovery) * Time.deltaTime);
            }
        }
    }

    public void Flap()
    {
        if (stamina >= 1.0 && animator.GetCurrentAnimatorStateInfo(0).IsName("Glide"))
        {
            rb.velocity = new Vector2(0.0f, (float)distance);
            animator.SetTrigger("Flap");
            animator.SetFloat("TimeSince", 0.0f);
            stamina--;
        }
    }

    public void Deficate()
    {
        if (guanoAmount >= 1)
        {
            rb.velocity = new Vector2(0.0f, (float)distance*0.3f);
            guanoAmount--;
            var guanoPosition = new Vector3(this.transform.position.x - 0.4f, this.transform.position.y - 0.75f, 0.0f);
            game.GetComponent<IMode>().ProduceGuano(guanoPosition);
            deficated = true;
        }

    }

    public void EatFood(GameObject food)
    {
        if (guanoAmount < 3)
        {
            guanoAmount++;
            game.GetComponent<IMode>().EatFood(food);
            eaten = food.GetComponent<Food>().uid;
            hasEaten = true;
        }
    }

    public void Hit()
    {
        if (stamina > 0.0)
        {
            stamina -= 0.8;
        }
        rb.velocity = new Vector2(rb.velocity.x - 0.1f, rb.velocity.y - 0.1f);
    }
}
