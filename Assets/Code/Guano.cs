using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guano : MonoBehaviour
{
    public GameObject game;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Translate((float)(-1 * game.GetComponent<IMode>().GetPipeSpeed() * Time.deltaTime), 0, 0, Space.World);
        if (this.gameObject.transform.position.x <= -10)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Splat();
        }
    }

    void Splat()
    {
        List<ContactPoint2D> contacts = new List<ContactPoint2D>();
        this.gameObject.GetComponent<Collider2D>().GetContacts(contacts);
        if (contacts.Count > 0)
        {
            // Find non-Ceiling contact
            foreach (ContactPoint2D contact in contacts)
            {
                GameObject hitObj = contact.collider.gameObject;
                if (hitObj.tag == "Player")
                {
                    var script = hitObj.GetComponent<Birb>();
                    script.Hit();
                    Destroy(this.gameObject);
                } else if (hitObj.tag == "Enemy")
                {
                    var script = hitObj.GetComponent<Enemy>();
                    script.Hit();
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
