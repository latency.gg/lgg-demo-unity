using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class MultiPlayerMenu : MonoBehaviour
{
    private void Start()
    {
        string UID = PlayerPrefs.GetString("sUID", "");
        if (UID == "")
        {
            SceneManager.LoadScene("PreferencesMenu");
        }
    }

    void Update()
    {        
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            QuitMenu();
        }
    }

    public void RaceGame()
    {
        MultiPlayerStatics.SelectedScene = "RaceModeScene";
        SceneManager.LoadScene("MatchMaking");
    }

    public void TimeTrialGame()
    {
        MultiPlayerStatics.SelectedScene = "TimeTrialModeScene";
        SceneManager.LoadScene("MatchMaking");
    }

    public void SurvivalGame()
    {
        MultiPlayerStatics.SelectedScene = "SurvivalModeScene";
        SceneManager.LoadScene("MatchMaking");
    }

    public void PreferencesMenu()
    {
        MultiPlayerStatics.SelectedScene = "PreferencesMenu";
        SceneManager.LoadScene("PreferencesMenu");
    }

    public void QuitMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
