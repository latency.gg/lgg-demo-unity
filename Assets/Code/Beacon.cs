using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : MonoBehaviour
{
    // Start is called before the first frame update
    LineRenderer lr;
    Double packetTarget = 1;
    Double animationTimeConsumed = 0;

    void Start()
    {
        GameObject circle = GameObject.Find("Probe");
        Material lineMat = new Material(Shader.Find("Unlit/Color"));
        Color lineColor = new Color(0.9059f, 0.7176f, 0.3961f, 1.0f);
        lineMat.SetColor("_Color", lineColor);
        lr = this.gameObject.AddComponent<LineRenderer>();
        lr.material = lineMat;
        lr.SetPosition(0, circle.transform.position);
        lr.SetPosition(1, this.gameObject.transform.position);
        lr.startWidth = .01f;
        lr.endWidth = .01f;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject circle = GameObject.Find("Probe");
        lr.SetPosition(0, circle.transform.position);
        lr.SetPosition(1, this.gameObject.transform.position);
        float distance = Vector3.Distance(circle.transform.position, this.gameObject.transform.position);
        Material beaconMat = new Material(Shader.Find("Unlit/Color"));
        Color beaconColorA = new Color(0.21f, 0.3098f, 0.4802f, 1.0f);
        Color beaconColorB = new Color(0.3098f, 0.4802f, 0.21f, 1.0f);
        Color beaconColor = Color.Lerp(beaconColorB, beaconColorA, distance/4.5f);
        beaconMat.SetColor("_Color", beaconColor);
        beaconMat.renderQueue = 4000;
        this.gameObject.GetComponent<Renderer>().material = beaconMat;
        updatePing();
    }

    private void updatePing()
    {
        Double beacon_distance = Math.Sqrt(Math.Pow(this.gameObject.transform.position.x, 2) + Math.Pow(this.gameObject.transform.position.y, 2));
        Double animation_time = ((beacon_distance-0.5)/(3.5-0.5)) * (1-0.2) + 0.2;
        Double percent_complete = animationTimeConsumed / animation_time;
        if (percent_complete >= 1)
        {
            animationTimeConsumed = 0;
            if (packetTarget >= 1)
            {
                packetTarget = 0;
            }
            else
            {
                packetTarget = 1;
            }
        }
        else
        {
            animationTimeConsumed += Time.deltaTime;
            percent_complete = animationTimeConsumed / animation_time;
        }
        Double radians = Math.Atan2(this.gameObject.transform.position.y, this.gameObject.transform.position.x*-1) - (Math.PI/2);
        Double degrees = Math.Atan2(this.gameObject.transform.position.y, this.gameObject.transform.position.x) * (180 / Math.PI) - 90;
        Double new_distance;
        if (packetTarget >= 1)
        {
            new_distance = beacon_distance * percent_complete;
        }
        else
        {
            new_distance = beacon_distance * (1-percent_complete);
        }
        Double x = new_distance * Math.Sin(radians);
        Double y = new_distance * Math.Cos(radians);
        GameObject capsule = this.gameObject.transform.GetChild(0).gameObject;
        capsule.transform.position = new Vector3((float)x, (float)y, (float)0);
        Vector3 rotation_vector = new Vector3((float)0, (float)0, (float)degrees);
        capsule.transform.localRotation = Quaternion.Euler(rotation_vector);
    }

    ~Beacon()
    {
        Destroy(this.gameObject);
    }
}
